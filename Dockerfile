# https://github.com/markpfletcher/docker-unison/blob/master/Dockerfile

FROM alpine:latest

RUN apk add --update --no-cache unison openssh

# set timezone
RUN apk add --update tzdata \
 && cp /usr/share/zoneinfo/Europe/Paris /etc/localtime \
 && echo "Europe/Paris" > /etc/timezone \
 && apk del tzdata

RUN set -x \
	&& deluser xfs \
	&& addgroup -g 33 -S www-data \
	&& adduser -u 1002 -D -S -G www-data deploy

# add cron jobs
RUN (crontab -l ; echo "* * * * * /usr/bin/unison &> /dev/null") | sort - | uniq - | crontab -

EXPOSE 22

CMD ["unison"]